[TheFish](https://codeberg.org/BSDforge/thefish)
=======

![The Fish](shark.gif) **The Fish** provides a graphical UI (with
[GTK+](http://www.gtk.org/), [Qt](https://www.qt.io), and
[ncurses](https://bsdforge.com/man/?ncurses) frotends) to manage
and edit system variables stored in
[`/etc/defaults/rc.conf`](https://bsdforge.com/man/?rc.conf) and
[`/etc/rc.conf`](https://bsdforge.com/man/?rc.conf).

The Fish is a [GTK+](http://www.gtk.org/), [Qt](https://www.qt.io),
and [ncurses](https://bsdforge.com/man/?ncurses) based
[`rc.conf`](https://bsdforge.com/man/?rc.conf) editor.

Although some people just like to use vi to change their
[`rc.conf`](https://bsdforge.com/man/?rc.conf) setup, I thought it
would be nice to have a graphical tool for those who feel better
using one, or who just started using [FreeBSD](https://FreeBSD.org).

This software is licensed under the friendly [BSD 2 clause license](LICENSE).

It's source code is hosted at:
[https://codeberg.org/BSDforge/thefish](https://codeberg.org/BSDforge/thefish)
